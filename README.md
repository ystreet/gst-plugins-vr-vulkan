# GStreamer VR Plugins

This repository contains GStreamer plugins for watching spherical video in VR.

## Disclaimer

Gst VR Plugins are in a very early development stage, you will get motion sick :)

## Dependencies

### VR Plugins

* GStreamer
* GStreamer Vulkan Plugins
* Meson
* graphene

## Build

```
meson _builddir
ninja -C _builddir
```

## Usage

### View spherical video on a DK2

```
gst-launch-1.0 filesrc location=~/video.webm ! decodebin ! vulkanupload ! vulkancolorconvert ! videorate ! vulkanvrcompositor ! video/x-raw\(ANY\), width=1920, height=1080, framerate=75/1 ! vulkansink
```

## License

LGPLv2
