/*
 * GStreamer Plugins Vulkan VR
 * Copyright (C) 2019 Matthew Waters <matthew@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gst3dnode.h"

#include "shaders/mvp_uv.vert.h"
#include "shaders/texture_uv.frag.h"

#define GST_CAT_DEFAULT gst_3d_node_debug
GST_DEBUG_CATEGORY_STATIC (GST_CAT_DEFAULT);

G_DEFINE_TYPE_WITH_CODE (Gst3DNode, gst_3d_node, GST_TYPE_VULKAN_FULL_SCREEN_QUAD,
    GST_DEBUG_CATEGORY_INIT (gst_3d_node_debug, "3dnode", 0, "node"));

struct uniform_data
{
  float mvp[16];
};

#define SPIRV_MAGIC_NUMBER_NE 0x07230203
#define SPIRV_MAGIC_NUMBER_OE 0x03022307

static gboolean
ensure_vertex_data (Gst3DNode * self, GError ** error)
{
  GstVulkanFullScreenQuad *quad = GST_VULKAN_FULL_SCREEN_QUAD (self);
  GstMapInfo map_info;

  g_return_val_if_fail (GST_IS_3D_MESH (self->mesh), FALSE);

  if (!self->vertices) {
    self->vertices = gst_vulkan_buffer_memory_alloc (quad->queue->device,
      self->mesh->vertices_size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (!gst_memory_map (self->vertices, &map_info, GST_MAP_WRITE)) {
      g_set_error_literal (error, GST_VULKAN_ERROR, VK_ERROR_MEMORY_MAP_FAILED,
          "Failed to map memory");
      goto failure;
    }

    memcpy (map_info.data, self->mesh->vertices, map_info.size);
    gst_memory_unmap (self->vertices, &map_info);
  }

  if (!self->indices) {
    self->indices = gst_vulkan_buffer_memory_alloc (quad->queue->device,
      self->mesh->indices_size, VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if (!gst_memory_map (self->indices, &map_info, GST_MAP_WRITE)) {
      g_set_error_literal (error, GST_VULKAN_ERROR, VK_ERROR_MEMORY_MAP_FAILED,
          "Failed to map memory");
      goto failure;
    }

    memcpy (map_info.data, self->mesh->indices, map_info.size);
    gst_memory_unmap (self->indices, &map_info);

    self->n_indices = self->mesh->n_indices;
  }

  return TRUE;

failure:
  if (self->vertices)
    gst_memory_unref (self->vertices);
  self->vertices = NULL;
  if (self->indices)
    gst_memory_unref (self->indices);
  self->indices = NULL;
  self->n_indices = 0;
  return FALSE;
}

gboolean
gst_3d_node_set_info (Gst3DNode * self, GstVideoInfo *in_info, GstVideoInfo * out_info)
{
  GstVulkanFullScreenQuad *quad = GST_VULKAN_FULL_SCREEN_QUAD (self);
  gst_vulkan_full_screen_quad_set_info (quad, in_info, out_info);
  self->state_outdated = TRUE;

  return TRUE;
}

void
gst_3d_node_init (Gst3DNode * self)
{
  self->state_outdated = TRUE;
}

Gst3DNode *
gst_3d_node_new (GstVulkanQueue * queue, Gst3DCamera * camera, Gst3DMesh * mesh)
{
  GstVulkanFullScreenQuad *quad;
  GstVulkanHandle *vert, *frag;
  Gst3DNode *node;

  g_return_val_if_fail (GST_IS_VULKAN_QUEUE (queue), NULL);
  g_return_val_if_fail (GST_IS_3D_CAMERA (camera), NULL);
  g_return_val_if_fail (GST_IS_3D_MESH (mesh), NULL);

  node = g_object_new (GST_3D_TYPE_NODE, NULL);
  node->camera = gst_object_ref (camera);
  node->mesh = gst_object_ref (mesh);

  quad = GST_VULKAN_FULL_SCREEN_QUAD (node);
  quad->queue = gst_object_ref (queue);

  ensure_vertex_data (node, NULL);
  gst_vulkan_full_screen_quad_set_vertex_buffer (quad, node->vertices, NULL);
  gst_vulkan_full_screen_quad_set_index_buffer (quad, node->indices, node->n_indices, NULL);

  vert =
      gst_vulkan_create_shader (quad->queue->device, mvp_uv_vert,
      mvp_uv_vert_size, NULL);
  frag =
      gst_vulkan_create_shader (quad->queue->device, texture_uv_frag,
      texture_uv_frag_size, NULL);
  gst_vulkan_full_screen_quad_set_shaders (quad, vert, frag);

  gst_vulkan_handle_unref (vert);
  gst_vulkan_handle_unref (frag);

  return node;
}

static void
gst_3d_node_finalize (GObject * object)
{
  Gst3DNode *self = GST_3D_NODE (object);

  gst_clear_mini_object (((GstMiniObject **) &self->vertices));
  gst_clear_mini_object (((GstMiniObject **) &self->indices));
  gst_clear_mini_object (((GstMiniObject **) &self->uniforms));

  gst_clear_object (&self->camera);
  gst_clear_object (&self->mesh);

  G_OBJECT_CLASS (gst_3d_node_parent_class)->finalize (object);
}

static void
gst_3d_node_class_init (Gst3DNodeClass * klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->finalize = gst_3d_node_finalize;
}

gboolean
gst_3d_node_set_input_buffer (Gst3DNode * self, GstBuffer * buffer, GError ** error)
{
  GstVulkanFullScreenQuad *quad = GST_VULKAN_FULL_SCREEN_QUAD (self);
  return gst_vulkan_full_screen_quad_set_input_buffer (quad, buffer, error);
}

gboolean
gst_3d_node_set_output_buffer (Gst3DNode * self, GstBuffer * buffer, GError ** error)
{
  GstVulkanFullScreenQuad *quad = GST_VULKAN_FULL_SCREEN_QUAD (self);
  return gst_vulkan_full_screen_quad_set_output_buffer (quad, buffer, error);
}

static gboolean
ensure_uniform_data (Gst3DNode * self, GError ** error)
{
  GstVulkanFullScreenQuad *quad = GST_VULKAN_FULL_SCREEN_QUAD (self);
  GstMapInfo map_info;
  struct uniform_data mvp;

  if (!self->uniforms) {
    self->uniforms = gst_vulkan_buffer_memory_alloc (quad->queue->device,
      self->mesh->vertices_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    if (!self->uniforms) {
      g_set_error (error, GST_VULKAN_ERROR, VK_ERROR_OUT_OF_HOST_MEMORY,
          "Failed to create uniform buffer");
      goto failure;
    }
  }

  if (!gst_memory_map (self->uniforms, &map_info, GST_MAP_WRITE)) {
    g_set_error_literal (error, GST_VULKAN_ERROR, VK_ERROR_MEMORY_MAP_FAILED,
        "Failed to map memory");
    goto failure;
  }

  gst_3d_camera_update_view (self->camera);
  graphene_matrix_to_float (&self->camera->mvp, mvp.mvp);

  memcpy (map_info.data, &mvp.mvp, sizeof (struct uniform_data));
  gst_memory_unmap (self->uniforms, &map_info);

  return TRUE;

failure:
  if (self->uniforms)
    gst_memory_unref (self->uniforms);
  self->uniforms = NULL;
  return FALSE;
}

gboolean
gst_3d_node_draw (Gst3DNode * self, GError ** error)
{
  GstVulkanFullScreenQuad *quad = GST_VULKAN_FULL_SCREEN_QUAD (self);

  if (!ensure_uniform_data (self, error))
    return FALSE;

  if (!gst_vulkan_full_screen_quad_set_uniform_buffer (quad, self->uniforms, error))
    return FALSE;

  return gst_vulkan_full_screen_quad_draw (quad, error);
}
