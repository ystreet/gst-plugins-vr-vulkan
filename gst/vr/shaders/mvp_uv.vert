#version 450 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec2 out_uv;

layout(set = 0, binding = 0) uniform MapUV {
  mat4 mvp;
};

void main()
{
   gl_Position = mvp * vec4(position, 1.0);
   out_uv = uv;
}

