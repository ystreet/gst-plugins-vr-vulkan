/*
 * GStreamer Plugins VR Vulkan
 * Copyright (C) 2019 Matthew Waters <matthew@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-vulkanvrcompositor
 *
 * Transform video for VR using Vulkan.
 *
 * <refsect2>
 * <title>Examples</title>
 * |[
 * gst-launch-1.0 filesrc location=~/Videos/360.webm ! decodebin ! vulkanupload ! vulkancolorconvert ! vulkanvrcompositor ! vulkansink
 * ]| Play spheric video.
 * </refsect2>
 */

#define GST_USE_UNSTABLE_API

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "gstvulkanvrcompositor.h"

#include <gst/vulkan/vulkan.h>
#include <graphene-gobject.h>

#include "gst3dnode.h"
#include "gst3dcamera_arcball.h"

#define GST_CAT_DEFAULT gst_vulkan_vr_compositor_debug
GST_DEBUG_CATEGORY_STATIC (GST_CAT_DEFAULT);

#define gst_vulkan_vr_compositor_parent_class parent_class

#define IMAGE_FORMATS " { BGRA, RGBA }"

static GstStaticPadTemplate gst_vulkan_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE_WITH_FEATURES
        (GST_CAPS_FEATURE_MEMORY_VULKAN_IMAGE,
            IMAGE_FORMATS)));

static GstStaticPadTemplate gst_vulkan_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE_WITH_FEATURES
        (GST_CAPS_FEATURE_MEMORY_VULKAN_IMAGE,
            IMAGE_FORMATS)));

enum
{
  PROP_0,
};

#define DEBUG_INIT \
    GST_DEBUG_CATEGORY_INIT (gst_vulkan_vr_compositor_debug, "vulkanvrcompositor", 0, "vulkanvrcompositor element");

G_DEFINE_TYPE_WITH_CODE (GstVulkanVRCompositor, gst_vulkan_vr_compositor,
    GST_TYPE_BASE_TRANSFORM, DEBUG_INIT);

static void gst_vulkan_vr_compositor_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_vulkan_vr_compositor_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static void gst_vulkan_vr_compositor_set_context (GstElement * element,
    GstContext * context);

static gboolean gst_vulkan_vr_compositor_query (GstBaseTransform * trans,
    GstPadDirection direction, GstQuery * query);
static gboolean gst_vulkan_vr_compositor_src_event (GstBaseTransform * trans,
    GstEvent * event);
static gboolean gst_vulkan_vr_compositor_set_caps (GstBaseTransform * filter,
    GstCaps * incaps, GstCaps * outcaps);
static gboolean gst_vulkan_vr_compositor_decide_allocation (GstBaseTransform * bt,
    GstQuery * query);
static gboolean gst_vulkan_vr_compositor_propose_allocation (GstBaseTransform * bt,
    GstQuery * decide_query, GstQuery * query);
static GstFlowReturn gst_vulkan_vr_compositor_transform (GstBaseTransform * bt,
    GstBuffer * inbuf, GstBuffer * outbuf);

static gboolean gst_vulkan_vr_compositor_start (GstBaseTransform * bt);
static gboolean gst_vulkan_vr_compositor_stop (GstBaseTransform * trans);


static void
gst_vulkan_vr_compositor_class_init (GstVulkanVRCompositorClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;
  GstBaseTransformClass *base_transform_class;

  gobject_class = (GObjectClass *) klass;
  element_class = GST_ELEMENT_CLASS (klass);
  base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class->set_property = gst_vulkan_vr_compositor_set_property;
  gobject_class->get_property = gst_vulkan_vr_compositor_get_property;

  base_transform_class->start = gst_vulkan_vr_compositor_start;
  base_transform_class->stop = gst_vulkan_vr_compositor_stop;
  base_transform_class->src_event = gst_vulkan_vr_compositor_src_event;
  base_transform_class->query = gst_vulkan_vr_compositor_query;
  base_transform_class->set_caps = gst_vulkan_vr_compositor_set_caps;
  base_transform_class->decide_allocation = gst_vulkan_vr_compositor_decide_allocation;
  base_transform_class->propose_allocation = gst_vulkan_vr_compositor_propose_allocation;
  base_transform_class->transform = gst_vulkan_vr_compositor_transform;

  element_class->set_context = gst_vulkan_vr_compositor_set_context;

  gst_element_class_set_metadata (element_class, "Vulkan VR compositor",
      "Filter/Effect/Video", "Transform video for VR using Vulkan",
      "Lubosz Sarnecki <lubosz.sarnecki@collabora.co.uk>\n"
      "Matthew Waters <matthew@centricular.com>");

  gst_element_class_add_static_pad_template (element_class,
      &gst_vulkan_sink_template);
  gst_element_class_add_static_pad_template (element_class,
      &gst_vulkan_src_template);
}

static void
gst_vulkan_vr_compositor_init (GstVulkanVRCompositor * self)
{
}

static void
gst_vulkan_vr_compositor_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_vulkan_vr_compositor_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
send_eos (GstElement * element)
{
  GstPad *sinkpad = gst_element_get_static_pad (element, "sink");
  if (sinkpad)
    gst_pad_send_event (sinkpad, gst_event_new_eos ());
  else {
    GstPad *srcpad = gst_element_get_static_pad (element, "src");
    gst_pad_send_event (srcpad, gst_event_new_flush_stop (FALSE));
  }
}

static void
on_navigation_event (GstVulkanVRCompositor * self, GstEvent * event)
{
  GstNavigationEventType event_type = gst_navigation_event_get_type (event);

  gst_3d_camera_navigation_event (self->camera, event);

  switch (event_type) {
    case GST_NAVIGATION_EVENT_KEY_PRESS:{
      GstStructure *structure =
          (GstStructure *) gst_event_get_structure (event);
      const gchar *key = gst_structure_get_string (structure, "key");
      if (g_strcmp0 (key, "Escape") == 0)
        send_eos (GST_ELEMENT (self));
      break;
    }
    default:
      break;
  }
}

static gboolean
gst_vulkan_vr_compositor_src_event (GstBaseTransform * trans, GstEvent * event)
{
  GstVulkanVRCompositor *self = GST_VULKAN_VR_COMPOSITOR (trans);
  GST_DEBUG_OBJECT (trans, "handling %s event", GST_EVENT_TYPE_NAME (event));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_NAVIGATION:
      event =
          GST_EVENT (gst_mini_object_make_writable (GST_MINI_OBJECT (event)));
      on_navigation_event (self, event);
      break;
    default:
      break;
  }
  return GST_BASE_TRANSFORM_CLASS (parent_class)->src_event (trans, event);
}

static gboolean
gst_vulkan_vr_compositor_query (GstBaseTransform * trans,
    GstPadDirection direction, GstQuery * query)
{
  GstVulkanVRCompositor *self = GST_VULKAN_VR_COMPOSITOR (trans);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CONTEXT:{
      if (gst_vulkan_handle_context_query (GST_ELEMENT (self), query,
              NULL, self->instance, self->device))
        return TRUE;

      if (gst_vulkan_queue_handle_context_query (GST_ELEMENT (self),
              query, self->queue))
        return TRUE;

      break;
    }
    default:
      break;
  }

  return GST_BASE_TRANSFORM_CLASS (parent_class)->query (trans, direction, query);
}

static void
gst_vulkan_vr_compositor_set_context (GstElement * element,
    GstContext * context)
{
  GstVulkanVRCompositor *self = GST_VULKAN_VR_COMPOSITOR (element);

  gst_vulkan_handle_set_context (element, context, NULL, &self->instance);

  GST_ELEMENT_CLASS (parent_class)->set_context (element, context);
}

static gboolean
gst_vulkan_vr_compositor_stop (GstBaseTransform * trans)
{
  GstVulkanVRCompositor *self = GST_VULKAN_VR_COMPOSITOR (trans);

  gst_clear_object (&self->camera);
  gst_clear_object (&self->node);

  gst_clear_object (&self->queue);
  gst_clear_object (&self->device);
  gst_clear_object (&self->instance);

  return TRUE;
}

struct choose_data
{
  GstVulkanVRCompositor *vr;
  GstVulkanQueue *queue;
};

static gboolean
_choose_queue (GstVulkanDevice * device, GstVulkanQueue * queue,
    struct choose_data *data)
{
  guint flags =
      device->physical_device->queue_family_props[queue->family].queueFlags;

  if ((flags & VK_QUEUE_GRAPHICS_BIT) != 0) {
    if (data->queue)
      gst_object_unref (data->queue);
    data->queue = gst_object_ref (queue);
    return FALSE;
  }

  return TRUE;
}

static GstVulkanQueue *
_find_graphics_queue (GstVulkanVRCompositor *vr)
{
  struct choose_data data;

  data.vr = vr;
  data.queue = NULL;

  gst_vulkan_device_foreach_queue (vr->device,
      (GstVulkanDeviceForEachQueueFunc) _choose_queue, &data);

  return data.queue;
}

static gboolean
gst_vulkan_vr_compositor_start (GstBaseTransform * bt)
{
  GstVulkanVRCompositor *vr = GST_VULKAN_VR_COMPOSITOR (bt);
  Gst3DMesh *mesh;

  if (!gst_vulkan_ensure_element_data (GST_ELEMENT (bt), NULL,
          &vr->instance)) {
    GST_ELEMENT_ERROR (vr, RESOURCE, NOT_FOUND,
        ("Failed to retrieve vulkan instance"), (NULL));
    return FALSE;
  }
  if (!gst_vulkan_device_run_context_query (GST_ELEMENT (vr),
          &vr->device)) {
    GError *error = NULL;
    GST_DEBUG_OBJECT (vr, "No device retrieved from peer elements");
    if (!(vr->device =
            gst_vulkan_instance_create_device (vr->instance, &error))) {
      GST_ELEMENT_ERROR (vr, RESOURCE, NOT_FOUND,
          ("Failed to create vulkan device"), ("%s", error->message));
      g_clear_error (&error);
      return FALSE;
    }
  }

  if (!gst_vulkan_queue_run_context_query (GST_ELEMENT (vr),
          &vr->queue)) {
    GST_DEBUG_OBJECT (vr, "No queue retrieved from peer elements");
    vr->queue = _find_graphics_queue (vr);
  }
  if (!vr->queue)
    return FALSE;

  mesh = gst_3d_mesh_new_sphere (800.0, 100, 100);
  vr->camera = GST_3D_CAMERA (gst_3d_camera_arcball_new ());
  vr->node = gst_3d_node_new (vr->queue, vr->camera, mesh);
  gst_object_unref (mesh);

  return TRUE;
}

static gboolean
gst_vulkan_vr_compositor_set_caps (GstBaseTransform * bt, GstCaps * incaps,
    GstCaps * outcaps)
{
  GstVulkanVRCompositor *self = GST_VULKAN_VR_COMPOSITOR (bt);
  GstVideoInfo in_info, out_info;

  if (!gst_video_info_from_caps (&in_info, incaps))
    return FALSE;
  if (!gst_video_info_from_caps (&out_info, outcaps))
    return FALSE;

  if (!gst_3d_node_set_info (self->node, &in_info, &out_info))
    return FALSE;

  return TRUE;
}

static gboolean
gst_vulkan_vr_compositor_propose_allocation (GstBaseTransform * bt,
    GstQuery * decide_query, GstQuery * query)
{
  /* FIXME: */
  return FALSE;
}

static gboolean
gst_vulkan_vr_compositor_decide_allocation (GstBaseTransform * bt,
    GstQuery * query)
{
  GstVulkanVRCompositor *vr = GST_VULKAN_VR_COMPOSITOR (bt);
  GstBufferPool *pool = NULL;
  GstStructure *config;
  GstCaps *caps;
  guint min, max, size;
  gboolean update_pool;

  gst_query_parse_allocation (query, &caps, NULL);
  if (!caps)
    return FALSE;

  if (gst_query_get_n_allocation_pools (query) > 0) {
    gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min, &max);

    update_pool = TRUE;
  } else {
    GstVideoInfo vinfo;

    gst_video_info_init (&vinfo);
    gst_video_info_from_caps (&vinfo, caps);
    size = vinfo.size;
    min = max = 0;
    update_pool = FALSE;
  }

  if (!pool || !GST_IS_VULKAN_IMAGE_BUFFER_POOL (pool)) {
    if (pool)
      gst_object_unref (pool);
    pool = gst_vulkan_image_buffer_pool_new (vr->device);
  }

  config = gst_buffer_pool_get_config (pool);

  gst_buffer_pool_config_set_params (config, caps, size, min, max);
  gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_VIDEO_META);

  gst_buffer_pool_set_config (pool, config);

  if (update_pool)
    gst_query_set_nth_allocation_pool (query, 0, pool, size, min, max);
  else
    gst_query_add_allocation_pool (query, pool, size, min, max);

  gst_object_unref (pool);

  return TRUE;
}

static GstFlowReturn
gst_vulkan_vr_compositor_transform (GstBaseTransform * bt, GstBuffer * inbuf,
    GstBuffer * outbuf)
{
  GstVulkanVRCompositor *self = GST_VULKAN_VR_COMPOSITOR (bt);
  GError *error = NULL;

  if (!gst_3d_node_set_input_buffer (self->node, inbuf, &error))
    goto error;
  if (!gst_3d_node_set_output_buffer (self->node, outbuf, &error))
    goto error;

  if (!gst_3d_node_draw (self->node, &error))
    goto error;

  return GST_FLOW_OK;

error:
  GST_ERROR_OBJECT (self, "Failed to render: %s", error->message);
  return GST_FLOW_ERROR;
}
