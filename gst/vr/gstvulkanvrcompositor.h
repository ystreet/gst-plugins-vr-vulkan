/*
 * GStreamer Plugins VR Vulkan
 * Copyright (C) 2019 Matthew Waters <matthew@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_VULKAN_VR_COMPOSITOR_H_
#define _GST_VULKAN_VR_COMPOSITOR_H_

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>

#include <graphene.h>
#include "gst3dnode.h"

G_BEGIN_DECLS
#define GST_TYPE_VULKAN_VR_COMPOSITOR            (gst_vulkan_vr_compositor_get_type())
#define GST_VULKAN_VR_COMPOSITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_VULKAN_VR_COMPOSITOR,GstVulkanVRCompositor))
#define GST_IS_VULKAN_VR_COMPOSITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_VULKAN_VR_COMPOSITOR))
#define GST_VULKAN_VR_COMPOSITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass) ,GST_TYPE_VULKAN_VR_COMPOSITOR,GstVulkanVRCompositorClass))
#define GST_IS_VULKAN_VR_COMPOSITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass) ,GST_TYPE_VULKAN_VR_COMPOSITOR))
#define GST_VULKAN_VR_COMPOSITOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj) ,GST_TYPE_VULKAN_VR_COMPOSITOR,GstVulkanVRCompositorClass))
typedef struct _GstVulkanVRCompositor GstVulkanVRCompositor;
typedef struct _GstVulkanVRCompositorClass GstVulkanVRCompositorClass;

struct _GstVulkanVRCompositor
{
  GstBaseTransform parent;

  GstVulkanInstance *instance;
  GstVulkanDevice *device;
  GstVulkanQueue *queue;

  Gst3DCamera *camera;
  Gst3DNode *node;
};

struct _GstVulkanVRCompositorClass
{
  GstBaseTransformClass parent_class;
};

GType gst_vulkan_vr_compositor_get_type (void);

G_END_DECLS
#endif /* _GST_VULKAN_VR_COMPOSITOR_H_ */
