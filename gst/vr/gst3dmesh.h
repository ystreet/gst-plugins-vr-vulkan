/*
 * GStreamer Plugins VR
 * Copyright (C) 2016 Lubosz Sarnecki <lubosz.sarnecki@collabora.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_3D_MESH_H__
#define __GST_3D_MESH_H__


#include <gst/gst.h>

G_BEGIN_DECLS
#define GST_3D_TYPE_MESH            (gst_3d_mesh_get_type ())
#define GST_3D_MESH(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_3D_TYPE_MESH, Gst3DMesh))
#define GST_3D_MESH_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_3D_TYPE_MESH, Gst3DMeshClass))
#define GST_IS_3D_MESH(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_3D_TYPE_MESH))
#define GST_IS_3D_MESH_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_3D_TYPE_MESH))
#define GST_3D_MESH_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_3D_TYPE_MESH, Gst3DMeshClass))
typedef struct _Gst3DMesh Gst3DMesh;
typedef struct _Gst3DMeshClass Gst3DMeshClass;

struct Vertex
{
  float x, y, z;
  float s, t;
};

struct _Gst3DMesh
{
  /*< private > */
  GstObject parent;

  gsize n_indices;

  gpointer vertices;
  gsize vertices_size;
  gpointer indices;
  gsize indices_size;
};

struct _Gst3DMeshClass
{
  GstObjectClass parent_class;
};

Gst3DMesh * gst_3d_mesh_new (void);
Gst3DMesh * gst_3d_mesh_new_sphere (float radius, unsigned stacks,
    unsigned slices);

GType gst_3d_mesh_get_type (void);

G_END_DECLS
#endif /* __GST_3D_MESH_H__ */
