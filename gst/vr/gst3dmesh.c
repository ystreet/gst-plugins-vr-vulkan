/*
 * GStreamer Plugins VR
 * Copyright (C) 2016 Lubosz Sarnecki <lubosz.sarnecki@collabora.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "gst3dmesh.h"

#define GST_CAT_DEFAULT gst_3d_mesh_debug
GST_DEBUG_CATEGORY_STATIC (GST_CAT_DEFAULT);

G_DEFINE_TYPE_WITH_CODE (Gst3DMesh, gst_3d_mesh, GST_TYPE_OBJECT,
    GST_DEBUG_CATEGORY_INIT (gst_3d_mesh_debug, "3dmesh", 0, "mesh"));

void
gst_3d_mesh_init (Gst3DMesh * self)
{
}

Gst3DMesh *
gst_3d_mesh_new (void)
{
  Gst3DMesh *mesh = g_object_new (GST_3D_TYPE_MESH, NULL);

  return mesh;
}

static void
gst_3d_mesh_upload_sphere (Gst3DMesh * self, float radius, unsigned stacks,
    unsigned slices)
{
  gsize n_vertices = (slices + 1) * stacks;
  const int vert_stride = sizeof (struct Vertex);
  self->vertices = g_malloc0_n (n_vertices, vert_stride);
  self->vertices_size = n_vertices * vert_stride;

  float *v = (float *) self->vertices;;

  float const J = 1. / (float) (stacks - 1);
  float const I = 1. / (float) (slices - 1);

  for (int i = 0; i < slices; i++) {
    float const theta = M_PI * i * I;
    for (int j = 0; j < stacks; j++) {
      float const phi = 2 * M_PI * j * J + M_PI / 2.0;

      float const x = sin (theta) * cos (phi);
      float const y = -cos (theta);
      float const z = sin (phi) * sin (theta);

      *v++ = x * radius;
      *v++ = y * radius;
      *v++ = z * radius;

      *v++ = j * J;
      *v++ = i * I;
    }
  }

  /* index */
  self->n_indices = (slices - 1) * stacks * 2;

  self->indices = (gushort *) g_malloc0_n (self->n_indices, sizeof (gushort));
  self->indices_size = self->n_indices * sizeof (gushort);

  gushort *indextemp = self->indices;

  // -3 = minus caps slices - one to iterate over strips
  for (int i = 0; i < slices - 1; i++) {
    for (int j = 0; j < stacks; j++) {
      *indextemp++ = i * stacks + j;
      *indextemp++ = (i + 1) * stacks + j;
    }
  }

  /* linear index */
  /*
     self->index_size = (slices - 2) * stacks;
     for (int i = 0; i < self->index_size; i++)
     *indextemp++ = i;
   */
}

Gst3DMesh *
gst_3d_mesh_new_sphere (float radius, unsigned stacks,
    unsigned slices)
{
  Gst3DMesh *mesh = gst_3d_mesh_new ();

  gst_3d_mesh_upload_sphere (mesh, radius, stacks, slices);

  return mesh;
}

static void
gst_3d_mesh_finalize (GObject * object)
{
  Gst3DMesh *self = GST_3D_MESH (object);

  g_free (self->vertices);
  g_free (self->indices);

  G_OBJECT_CLASS (gst_3d_mesh_parent_class)->finalize (object);
}

static void
gst_3d_mesh_class_init (Gst3DMeshClass * klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->finalize = gst_3d_mesh_finalize;
}
