/*
 * GStreamer Plugins VR
 * Copyright (C) 2019 Matthew Waters <matthew@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_3D_NODE_H__
#define __GST_3D_NODE_H__


#include <gst/gst.h>

#include <gst/vulkan/vulkan.h>

#include "gst3dmesh.h"
#include "gst3dcamera.h"

G_BEGIN_DECLS

GType gst_3d_node_get_type (void);
#define GST_3D_TYPE_NODE            (gst_3d_node_get_type ())
#define GST_3D_NODE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_3D_TYPE_NODE, Gst3DNode))
#define GST_3D_NODE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_3D_TYPE_NODE, Gst3DNodeClass))
#define GST_IS_3D_NODE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_3D_TYPE_NODE))
#define GST_IS_3D_NODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_3D_TYPE_NODE))
#define GST_3D_NODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_3D_TYPE_NODE, Gst3DNodeClass))

typedef struct _Gst3DNode Gst3DNode;
typedef struct _Gst3DNodeClass Gst3DNodeClass;

struct _Gst3DNode
{
  GstVulkanFullScreenQuad           parent;

  Gst3DCamera                      *camera;
  Gst3DMesh                        *mesh;

  gboolean                          state_outdated;

  GstMemory                        *vertices;
  GstMemory                        *indices;
  gsize                             n_indices;
  GstMemory                        *uniforms;
};

struct _Gst3DNodeClass
{
  GstVulkanFullScreenQuadClass                    parent_class;
};

Gst3DNode *         gst_3d_node_new                 (GstVulkanQueue * queue, Gst3DCamera * camera, Gst3DMesh * mesh);

gboolean            gst_3d_node_set_info            (Gst3DNode * self, GstVideoInfo *in_info, GstVideoInfo * out_info);

gboolean            gst_3d_node_set_input_buffer    (Gst3DNode * self, GstBuffer * buffer, GError ** error);
gboolean            gst_3d_node_set_output_buffer   (Gst3DNode * self, GstBuffer * buffer, GError ** error);
gboolean            gst_3d_node_draw                (Gst3DNode * self, GError ** error);

G_END_DECLS
#endif /* __GST_3D_NODE_H__ */
