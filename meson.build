project('gst-plugins-vr-vulkan', 'c',
  version : '0.1',
  meson_version : '>= 0.47',
  default_options : [ 'warning_level=1',
                      'buildtype=debugoptimized' ])

gst_major = 1
gst_minor = 17
gst_micro = 0
gst_nano = 0

GST_DEP_VERSION = '>= @0@.@1@'.format(gst_major, gst_minor)

gst_version = '@0@.@1@.@2@'.format(gst_major, gst_minor, gst_micro)
apiversion = '1.0'
soversion = 0

prefix = get_option('prefix')

cc = meson.get_compiler('c')
libm = cc.find_library('m', required : false)

gobject_dep = dependency('gobject-2.0')
graphene_dep = dependency('graphene-1.0', fallback : ['graphene', 'graphene_dep'])
glib_dep = dependency('glib-2.0')
gst_dep = dependency('gstreamer-' + apiversion, version: GST_DEP_VERSION)
gst_vulkan_dep = dependency('gstreamer-vulkan-' + apiversion, version: GST_DEP_VERSION)
gst_video_dep = dependency('gstreamer-video-' + apiversion, version: GST_DEP_VERSION)

host_system = host_machine.system()

if ['ios', 'darwin'].contains(host_system)
  # gst-plugins-bad need to link against MoltenVK directly for the following reasons
  # - ios does not support the loader/validation layers
  # - We need to link directly to MoltenVK to be able to use
  #   MoltenVK-specific functions that use dispatchable handles (like
  #   retrieving the metal device from the VkDevice) which is currently waiting
  #   on implementing a proper Metal extension for Vulkan
  #   https://github.com/KhronosGroup/MoltenVK/issues/492
  vulkan_dep = cc.find_library('MoltenVK', required : true)
elif host_system == 'windows'
  vulkan_root = run_command(python3, '-c', 'import os; print(os.environ.get("VK_SDK_PATH"))').stdout().strip()
  if vulkan_root != '' and vulkan_root != 'None'
    vulkan_lib_dir = ''
    if build_machine.cpu_family() == 'x86_64'
      vulkan_lib_dir = join_paths(vulkan_root, 'Lib')
    else
      vulkan_lib_dir = join_paths(vulkan_root, 'Lib32')
    endif

    vulkan_lib = cc.find_library('vulkan-1', dirs: vulkan_lib_dir,
                                 required : true)

    vulkan_inc_dir = join_paths(vulkan_root, 'Include')
    has_vulkan_header = cc.has_header('vulkan/vulkan_core.h',
                                      args: '-I' + vulkan_inc_dir)

    if vulkan_lib.found() and has_vulkan_header
      vulkan_dep = declare_dependency(include_directories: include_directories(vulkan_inc_dir),
                                      dependencies: vulkan_lib)
    endif
  endif
else
  vulkan_dep = dependency('vulkan', method: 'pkg-config', required : false)
  if not vulkan_dep.found()
    vulkan_dep = cc.find_library('vulkan', required : false)
  endif
endif

gst_c_args = ['-DHAVE_CONFIG_H',
  '-DGST_DISABLE_DEPRECATED',
]

# Disable strict aliasing
if cc.has_argument('-fno-strict-aliasing')
  add_project_arguments('-fno-strict-aliasing', language: 'c')
endif

plugins_install_dir = join_paths(get_option('libdir'), 'gstreamer-1.0')
plugins_pkgconfig_install_dir = join_paths(plugins_install_dir, 'pkgconfig')
if get_option('default_library') == 'shared'
  # If we don't build static plugins there is no need to generate pc files
  plugins_pkgconfig_install_dir = disabler()
endif

cdata = configuration_data()
cdata.set('GST_API_VERSION', '"@0@"'.format(apiversion))
cdata.set('GST_DATADIR', '"@0@/@1@"'.format(prefix, get_option('datadir')))
cdata.set('LOCALEDIR', '"@0@/@1@"'.format(prefix, get_option('localedir')))
cdata.set('LIBDIR', '"@0@/@1@"'.format(prefix, get_option('libdir')))
cdata.set('GST_API_VERSION', '"' + apiversion + '"')
cdata.set('GETTEXT_PACKAGE', '"gstreamer' + apiversion + '"')
cdata.set('GST_LEVEL_DEFAULT', 'GST_LEVEL_NONE')
cdata.set('GST_LICENSE', '"LGPL"')
cdata.set('GST_PACKAGE_ORIGIN', '"https://gitlab.freedesktop.org/ystreet/gst-plugins-vr-vulkan"')
cdata.set('GST_PACKAGE_NAME', '"GStreamer Plugins VR Vulkan source release"')
cdata.set('PACKAGE', '"gst-plugins-vr-vulkan"')
cdata.set('PACKAGE_NAME', '"GStreamer Plugins VR Vulkan"')
cdata.set('PACKAGE_STRING', '"GStreamer Plugins VR Vulkan @0@"'.format(gst_version))
cdata.set('PACKAGE_TARNAME', '"gstreamer-plugins-vr-vulkan"')
cdata.set('PACKAGE_BUGREPORT', '"https://gitlab.freedesktop.org/ystreet/gst-plugins-vr-vulkan"')
cdata.set('PACKAGE_URL', 'https://gitlab.freedesktop.org/ystreet/gst-plugins-vr-vulkan""')
cdata.set('PACKAGE_VERSION', '"@0@"'.format(gst_version))
cdata.set('PLUGINDIR', '"@0@"'.format(plugins_install_dir))
cdata.set('VERSION', '"@0@"'.format(gst_version))

configinc = include_directories('.')

configure_file(input : 'config.h.meson',
  output : 'config.h',
  configuration : cdata)

pkgconfig = import('pkgconfig')

subdir('gst/vr')
